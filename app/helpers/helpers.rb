require 'net/smtp'
require 'open3'
require 'English'

class CptUtils

  helpers do

    def btrace_url(uuid)
      url(['backtrace', [uuid, 'html'].join('.')].join('/'))
    end

    def backtrace_email(address_to, address_from, uuid)
      message = <<-END.gsub(/^ {8}/, '')
        From: cptutils-online <#{address_from}>
        To: cptutils-online maintainer <#{address_to}>
        Subject: New backtrace

        Greetings

        cptutils-online at has generated a new backtrace

          #{btrace_url(uuid)}

        you might want to check it out.

        x
        END
      begin
        Net::SMTP.start('localhost') do |smtp|
          smtp.send_message(message, address_from, address_to)
        end
      rescue Errno::ECONNREFUSED => e
        logger.warn('failed to send backtrace email')
        logger.warn(e.message)
      end
    end

    def convert_error(ifmt, ofmt, uuid)
      sprintf("conversion of %s to %s failed (<a href='%s'>details</a>)",
              ifmt, ofmt, btrace_url(uuid))
    end

    def system_with_logging(*cmd)
      Open3.popen3(*cmd) do |stdin, stdout, stderr, wait|
        stdout_thread = Thread.new do
          Thread.current.abort_on_exception = true
          while line = stdout.gets
            logger.info(line.chomp)
          end
        end
        stderr_thread = Thread.new do
          Thread.current.abort_on_exception = true
          while line = stderr.gets
            logger.warn(line.chomp)
          end
        end
        stdout_thread.join
        stderr_thread.join
        wait.value == 0
      end
    end

    def formats(caps, io)
      format_data = caps.map do |type, grad|
        { desc: grad['desc'], type: type } if grad[io]
      end
      format_data.compact.sort_by { |f| f[:desc] }
    end

    def alias_dict(caps)
      toes = {}
      caps.each_pair do |type, grad|
        toes[type] = type
        toes[grad['ext']] = type
        grad['alias'].each { |ext| toes[ext] = type }
      end
      toes
    end

    def magic_dict(caps)
      len = 0
      dict = caps.each_with_object(Hash.new) do |(k, v), result|
        if magic = v['magic'] then
          result[k] = magic
          len = [len, magic.size].max
        end
      end
      {
        'max_len' => len,
        'magic' => dict
      }
    end

    def extensions(caps)
      Hash[caps.map { |type, grad| [type, grad['ext']] }]
    end

    def types_rw(caps, rw)
      Set.new(caps.map { |type, grad| type if grad[rw] }.compact)
    end

    def types_burstable(caps)
      Set.new(caps.map { |type, grad| type if grad['burst'] }.compact)
    end

    def sanitise(str)
      str.gsub(/[ '"+]/, '_')
    end

    def derived_basename(file)
      unsafe_base = File.basename(file, File.extname(file))
      sanitise(unsafe_base)
    end

    def derived_filename(file, ext)
      base = derived_basename(file)
      [base, ext].join('.')
    end

    def conversion_description(input, output)
      "conversion of #{input} colour gradient to #{output}"
    end

    def get_io_types(p)
      [p[:input]  || 'cpt', p[:output] || 'svg']
    end

    def js_path_build(base)
      "/js/#{base}.js"
    end

    def js_path(script)
      case script
      when :app then js_path_build('app')
      when :bstrap then js_path_build('bootstrap.min')
      else js_path_build(script.to_s)
      end
    end

    def link_to(target, text, cls: nil)
      if cls then
        "<a href='#{url(target)}' class='#{cls}'>#{text}</a>"
      else
        "<a href='#{url(target)}'>#{text}</a>"
      end
    end

    def link(href, **kwarg)
      attr = kwarg.map { |k, v| "#{k}='#{v}'" }.join(' ')
      "<link href='#{url(href)}' #{attr}>"
    end
  end
end
