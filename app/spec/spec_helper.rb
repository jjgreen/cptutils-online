ENV['RACK_ENV'] = 'test'

require 'rspec'

require_relative '../app'
CptUtils.set(:config, 'config.yaml')

require 'rack/test'
require 'capybara/rspec'

def app
  CptUtils.new
end

def preferred_extension(caps)
  Hash[ caps.map { |type, grad| [type, grad['ext']] } ]
end

def write_desc_hash(caps)
  desc_hash(caps, 'write')
end

def read_desc_hash(caps)
  desc_hash(caps, 'read')
end

def desc_hash(caps, read_write)
  pairs = caps.map do
    |type, grad| [type, grad['desc']] if grad[read_write]
  end
  Hash[ pairs.compact ]
end

def fixture(file)
  File.join(Dir.pwd, 'spec', 'fixtures', file)
end

RSpec.configure do |config|
  # disable deprecated 'should' syntax
  config.expect_with :rspec do |c|
    c.syntax = [:expect]
  end
  config.include Rack::Test::Methods
  config.include Capybara::DSL
  Capybara.app = app
end
