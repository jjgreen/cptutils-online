require 'spec_helper'

describe 'cptutils-online' do

  describe "basic features" do

    it "has an 'about' page" do
      get '/about.html'
      expect(last_response).to be_ok
      expect(last_response.body).to match /about this site/
    end

    it "redirects '/' to '/convert/select.html'" do
      get '/'
      expect(last_response).to be_redirect
      expect(last_response.location).to include '/convert/select.html'
    end

    it "redirects '/convert' to '/convert/select.html'" do
      get '/convert'
      expect(last_response).to be_redirect
      expect(last_response.location).to include '/convert/select.html'
    end

    it "redirects '/select.html' to '/convert/select.html'" do
      get '/select.html'
      expect(last_response).to be_redirect
      expect(last_response.location).to include '/convert/select.html'
    end

    it 'returns a 404 for unknown route' do
      get '/no/such/path'
      expect(last_response).to be_not_found
    end

    it "returns a 500 for the special '/crash.html' route" do
      get '/crash.html'
      expect(last_response).to be_server_error
    end
  end

  describe 'sitemap' do

    it "has a sitemap at '/sitemap.xml'" do
      get '/sitemap.xml'
      expect(last_response).to be_ok
    end

    it 'retrieved sitemap is valid' do
      get '/sitemap.xml'
      schema = Nokogiri::XML::Schema(File.open('spec/xsd/sitemap.xsd'))
      doc = Nokogiri::XML(last_response.body)
      schema.validate(doc).each do |error|
        puts error.message
      end
      expect(schema.validate(doc).length).to eq 0
    end

  end

  describe 'convert' do

    describe 'handling filenames' do

      it 'recognises jgd as an alias for PspGradient' do
        visit '/convert/select.html'
        attach_file 'upload', fixture('test.jgd')
        select 'PaintShop Pro gradient', from: 'select_input'
        select 'SVG gradient', from: 'select_output'
        click_button 'convert'
        expect(current_path).to eq '/convert/download.html'
        expect(page).to have_content 'Download link:'
      end

      it 'sanitises filenames with spaces' do
        visit '/convert/select.html'
        attach_file 'upload', fixture('zła nazwa pliku.gpl')
        select 'GIMP palette', from: 'select_input'
        select 'SVG gradient', from: 'select_output'
        click_button 'convert'
        expect(current_path).to eq '/convert/download.html'
        expect(page).to have_content 'Download link:'
        expect(page).to have_link 'zła_nazwa_pliku.svg'
      end

      it 'sanitises filenames with +' do
        visit '/convert/select.html'
        attach_file 'upload', fixture('zła+nazwa+pliku.gpl')
        select 'GIMP palette', from: 'select_input'
        select 'SVG gradient', from: 'select_output'
        click_button 'convert'
        expect(current_path).to eq '/convert/download.html'
        expect(page).to have_content 'Download link:'
        expect(page).to have_link 'zła_nazwa_pliku.svg'
      end

      it 'percent-encodes download urls' do
        visit '/convert/select.html'
        attach_file 'upload', fixture('percent#encode#me.gpl')
        select 'GIMP palette', from: 'select_input'
        select 'SVG gradient', from: 'select_output'
        click_button 'convert'
        expect(current_path).to eq '/convert/download.html'
        expect(page).to have_content 'Download link:'
        expect(page).to have_link 'percent#encode#me.svg'
        click_on 'percent#encode#me.svg'
        expect(page.current_path.split('/').last).
          to eq 'percent%23encode%23me.svg'
      end
    end

    describe 'when passed a fake grd file' do

      before do
        allow(Net::SMTP).to receive(:start)
        visit '/convert/select.html'
        attach_file 'upload', fixture('not-a-grd.grd')
        select 'Photoshop gradient', from: 'select_input'
        select 'CSS3 gradient', from: 'select_output'
        click_button 'convert'
      end

      it 'informs the user that an error occurred' do
        expect(current_path).to eq '/convert/select.html'
        expect(page).to_not have_content 'Download link:'
        expect(page).to have_content 'Error: conversion of grd to c3g failed'
      end

      it 'has a link to a detailed backtrace page' do
        expect(page).to have_link 'details'
        click_link 'details'
        expect(page).to have_content 'not a Photoshop GRD file'
      end
    end

    describe 'zipped checkbox' do

      before do
        visit '/convert/select.html'
      end

      it 'is unchecked by default' do
        expect(page).to_not have_checked_field 'zipped'
      end

      describe 'when checked' do

        before do
          check 'zipped'
          expect(page).to have_checked_field 'zipped'
        end

        it 'persists for multi-file format' do
          attach_file 'upload', fixture('test.svg')
          select 'SVG gradient',  from: 'select_input'
          select 'CSS3 gradient', from: 'select_output'
          click_button 'convert'
          expect(current_path).to eq '/convert/download.html'
          expect(page).to have_checked_field 'zipped'
        end

        it 'does not persist for single-file format' do
          attach_file 'upload', fixture('test.ggr')
          select 'GIMP gradient', from: 'select_input'
          select 'CSS3 gradient', from: 'select_output'
          click_button 'convert'
          expect(current_path).to eq '/convert/download.html'
          expect(page).to_not have_checked_field 'zipped'
        end
      end
    end

    describe 'corpus' do

      before(:all) do
        caps = YAML.load(`gradient-convert --capabilities`)
        @ext = preferred_extension(caps)
      end

      # would be nice to autogenerate these, but they are
      # parsed before the before block is run, so that
      # fails ...

      {
        'c3g' => 'CSS3 gradient',
        'cpt' => 'GMT colour palette table',
        'ggr' => 'GIMP gradient',
        'gpf' => 'Gnuplot palette',
        'inc' => 'POV-Ray header',
        'lut' => 'Medcon lookup table',
        'png' => 'PNG image',
        'psp' => 'PaintShop Pro gradient',
        'pg'  => 'PostGIS colour map',
        'qgs' => 'QGIS style colour-ramp',
        'sao' => 'DS9/SAO colour table',
        'svg' => 'SVG gradient',
        'map' => 'Tecplot map'
      }.each do |wfmt, wtext|
        {
          'cpt' => 'GMT colour palette table',
          'ggr' => 'GIMP gradient',
          'gpl' => 'GIMP palette',
          'grd' => 'Photoshop gradient',
          'psp' => 'PaintShop Pro gradient',
          'svg' => 'SVG gradient'
        }.each do |rfmt, rtext|
          next if rtext == wtext

          describe "#{rtext} to #{wtext}" do

            before do
              @rext = @ext[rfmt]
              @wext = @ext[wfmt]
              visit '/convert/select.html'
              attach_file 'upload', fixture("test.#{@rext}")
              select rtext, from: 'select_input'
              select wtext, from: 'select_output'
            end

            after do
              expect(current_path).to eq '/convert/download.html'
              expect(page).to have_content 'Download link:'
            end

            it 'converts to single file if zipped unchecked' do
              uncheck 'zipped'
              click_button 'convert'
              expect(page).to have_link "test.#{@wext}"
            end

            if ['grd', 'svg'].include?(rfmt) then
              it 'converts to zipped file if zipped checked' do
                check 'zipped'
                click_button 'convert'
                expect(page).to have_link "test_#{wfmt}.zip"
              end
            else
              it 'converts to single file if zipped checked' do
                check 'zipped'
                click_button 'convert'
                expect(page).to have_link "test.#{@wext}"
              end
            end
          end
        end
      end
    end
  end

  describe 'regressions' do

    context 'visiting download without session data' do

      it 'redirects to select' do
        get '/convert/download.html'
        expect(last_response).to be_redirect
        expect(last_response.location).to include '/convert/select.html'
      end
    end

    context 'No SMTP server' do
      before do
        allow(Net::SMTP)
          .to receive(:start)
                .and_raise(Errno::ECONNREFUSED, 'no SMTP')
        visit '/convert/select.html'
        attach_file 'upload', fixture('not-a-grd.grd')
        select 'Photoshop gradient', from: 'select_input'
        select 'CSS3 gradient', from: 'select_output'
        click_button 'convert'
      end

      it 'redirects to select' do
        expect(current_path).to eq '/convert/select.html'
      end
    end
  end
end
