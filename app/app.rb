#!/usr/bin/ruby

require 'sinatra/base'

class CptUtils < Sinatra::Base ; end

require 'xml-sitemap'
require 'tempfile'
require 'yaml'
require 'json'
require 'rack-flash'
require 'uri'
require 'cgi'
require 'uuidtools'

require_relative 'helpers/helpers'

class CptUtils
  use Rack::Flash

  configure :production, :development do
    enable :logging
  end

  configure do
    set :public_folder, 'public'
    set :static_cache_control, [:public, max_age: 60 * 60 * 24]
    set :sessions, same_site: :strict
    set :erb, trim: '-'
  end

  def initialize(arg=nil)
    super arg

    config = YAML.load_file(settings.config)
    @maintainer = config['maintainer']
    @sitemap = config['sitemap_path']
    @resdir = config['results_dir']
    @faildir = config['failed_dir']
    @cachedir = config['cache_dir']
    @btdir = config['backtrace_dir']
    @gconv = config['gradient_convert']
    @envir = config['environment']

    CptUtils.set(:environment, @envir.to_sym)

    if log_dir = config['log_dir'] then
      log_path = File.join(log_dir, 'cptutils-online.log')
      log = File.new(log_path, 'a')
      STDERR.reopen(log)
    end

    @sitemap = File.join(@cachedir, 'sitemap.xml')
    FileUtils.rm_f @sitemap

    @version = %x[#{@gconv} --version].split.last

    caps = YAML.load( %x[#{@gconv} --capabilities] )

    @read_formats = formats(caps, 'read')
    @write_formats = formats(caps, 'write')

    @navs = [
      { url: '/convert/select.html', text: 'convert' },
      { url: '/about.html', text: 'about' }
    ]

    @pvtype = 'png'
    @prev_h = 20
    @prev_w = 500
    @geom = "#{@prev_w}x#{@prev_h}"

    @alias_dict = alias_dict(caps)
    @magic_dict = magic_dict(caps)

    @burst_types = types_burstable(caps)
    @exts = extensions(caps)
  end

  helpers do

    def set_js(*scripts)
      @js =
        if scripts then
          scripts.map { |s| js_path(s) }.uniq
        end
    end

    def set_navs(url = nil)
      if url.nil? then
        @navs.each { |nav| nav[:class] = '' }
      else
        @navs.each { |nav| nav[:class] = (nav[:url] == url ? 'active' : '') }
      end
    end

    def sitemap(url)
      unless File.exist? @sitemap then
        map = XmlSitemap::Map.new(url) do |m|
          m.add '/convert/select.html'
          m.add '/about.html'
        end
        map.render_to(@sitemap)
      end
      @sitemap
    end
  end

  post '/convert' do

    ifmt = params[:input]
    ofmt = params[:output]

    session[:input] = ifmt
    session[:output] = ofmt
    session[:zipped] = !! params[:zipped]

    unless params[:upload] then
      redirect url('/convert/select.html')
    end

    upload_file = derived_filename(params[:upload][:filename], @exts[ifmt])
    logger.info "upload filename is #{upload_file}"

    host = request.env['HTTP_HOST'].split(':').first
    bt_email_to = @maintainer
    bt_email_from = "cptutils-online@#{host}"

    Dir.mktmpdir do |dir|

      # get upload path
      upload_path = File.join(dir, upload_file)
      File.open(upload_path, 'w') do |f|
        f.write(params[:upload][:tempfile].read)
      end
      unless File.exist? upload_path then
        logger.warn "failed write of upload to #{upload_path}"
        flash[:errors] = 'nothing was uploaded'
        redirect url('/convert/select.html')
      end

      # copy of input which is deleted on success
      failed_path = File.join(@faildir, upload_file)
      FileUtils.cp(upload_path, failed_path)

      # convert
      if session[:zipped] && @burst_types.include?(ifmt) then
        # multi
        basename = [derived_basename(upload_file), ofmt].join('_')
        download_file = [basename, 'zip'].join('.')
        download_path = File.join(@resdir, download_file)
        btrace_file = UUIDTools::UUID.random_create
        btrace_path = File.join(@btdir, btrace_file)
        File.unlink download_path if File.exist? download_path
        logger.info "download path is #{download_path}"
        logger.info "backtrace path is #{btrace_path}"
        cmd = [
          @gconv,
          '--verbose',
          '--preview',
          '--input-format', ifmt,
          '--output-format', ofmt,
          '--zip',
          '--backtrace-file', btrace_path,
          '--backtrace-format', 'json',
          upload_path,
          download_path
        ]
        unless system_with_logging(*cmd) then
          logger.warn "failed command #{cmd.join(' ')}"
          flash[:errors] = convert_error(ifmt, ofmt, btrace_file)
          backtrace_email(bt_email_to, bt_email_from, btrace_file)
          redirect url('/convert/select.html')
        end
      else
        # single
        download_file = derived_filename(upload_file, @exts[ofmt])
        download_path = File.join(@resdir, download_file)
        btrace_file = UUIDTools::UUID.random_create
        btrace_path = File.join(@btdir, btrace_file)
        logger.info "download path is #{download_path}"
        logger.info "backtrace path is #{btrace_path}"
        cmd = [
          @gconv,
          '--verbose',
          '--input-format', ifmt,
          '--output-format', ofmt,
          '--backtrace-file', btrace_path,
          '--backtrace-format', 'json',
          upload_path,
          download_path
        ]
        unless system_with_logging(*cmd) && File.exist?(download_path) then
          logger.warn "failed command #{cmd.join(' ')}"
          flash[:errors] = convert_error(ifmt, ofmt, btrace_file)
          backtrace_email(bt_email_to, bt_email_from, btrace_file)
          redirect url('/convert/select.html')
        end
        preview_file = derived_filename(upload_file, @pvtype)
        preview_path = File.join(@resdir, preview_file)
        cmd = [
          @gconv,
          '--verbose',
          '--geometry', @geom,
          '--input-format', ifmt,
          '--output-format', @pvtype,
          upload_path,
          preview_path
        ]
        system_with_logging(*cmd)
      end
      FileUtils.rm_f(failed_path)
      session[:download] = download_file
      redirect url('/convert/download.html')
    end
  end

  get '/' do
    redirect url('/convert/select.html')
  end

  get '/convert' do
    redirect url('/convert/select.html')
  end

  get '/select.html' do
    # remove this at some point, just for the namespacing of
    # convert to bed-in
    redirect url('/convert/select.html')
  end

  get '/about.html' do
    set_navs '/about.html'
    @keywords = ['about']
    @title = ['about']
    erb :about
  end

  get '/convert/download.html' do
    @input, @output = get_io_types(session)
    @download = Hash.new
    unless @download[:raw] = session[:download] then
      redirect url('/convert/select.html')
    end
    unless @download[:percent] = CGI.escape(@download[:raw]) then
      # FIXME should be a 500
      redirect url('/convert/select.html')
    end
    @preview = Hash.new
    [:raw, :percent].each do |type|
      unless File.extname(@download[type]) == '.zip' then
        @preview[type] = derived_filename(@download[type], @pvtype)
        session[:zipped] = false
      end
    end
    set_navs '/convert/select.html'
    set_js :app
    @keywords = [@input, @output]
    @title = ['download']
    erb :convert_download
  end

  get '/download/:download' do
    download = File.basename(params[:download]) or not_found
    path = File.join(@resdir, download)
    unless File.exist? path then
      logger.warn "no such file #{path}"
      not_found
    end
    send_file(
      path, {
        disposition: 'attachment',
        type: 'application/octet-stream',
        filename: download
      }
    )
  end

  get '/convert/select.html' do
    @input, @output = get_io_types(session)
    @upload = session[:upload]
    set_navs '/convert/select.html'
    set_js :app
    @keywords = [@input, @output]
    @description = conversion_description(@input, @output)
    @title = ['select']
    erb :convert_select
  end

  get '/img/preview/:file' do
    preview = File.basename( params[:file] ) or not_found
    path = File.join(@resdir, preview)
    unless File.exist? path then
      logger.warn "no such file #{path}"
      not_found
    end
    send_file(
      path, {
        disposition: 'inline',
        filename: preview
      }
    )
  end

  get '/backtrace/:uuid.html' do
    uuid =
      begin
        UUIDTools::UUID.parse(params[:uuid])
      rescue StandardError
        nil
      end
    unless uuid && uuid.valid? then
      logger.warn "backtrace for invalid uuid #{params[:uuid]}"
      not_found
    end
    path = File.join(@btdir, uuid.to_s)
    unless File.exist? path then
      logger.warn "no such file #{path}"
      not_found
    end
    unless json = File.read(path, encoding: 'utf-8') then
      logger.warn "failed to read from #{path}"
      not_found
    end
    unless @btrace = JSON.parse(json, symbolize_names: true) then
      logger.warn "failed parse of #{path}"
      not_found
    end
    set_navs '/convert/select.html'
    erb :backtrace
  end

  get '/sitemap.xml' do
    url = request.env['HTTP_HOST'] + request.env['SCRIPT_NAME']
    send_file(
      sitemap(url), {
        type: 'application/xml',
        filename: 'sitemap.xml'
      }
    )
  end

  get '/crash.html' do
    raise RuntimeError, 'deliberate crash triggered'
  end

  get '/*' do
    not_found
  end

  not_found do
    status 404
    set_navs
    erb :not_found
  end

  error do
    @e = request.env['sinatra_error']
    set_navs
    erb :crash
  end

end
