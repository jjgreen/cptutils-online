default: run

run:
	cd app ; bundle exec rackup --host '0.0.0.0'

test: test-js test-app

test-js:
	cd app ; eslint -c spec/eslintrc.json public/js/app.js

test-app:
	cd app ; bundle exec rspec -c -fd spec/app_spec.rb

deploy:
	cd ansible ; $(MAKE) app-production

clean:
	$(RM) app/public/results/* \
	      app/tmp/failed/* \
	      app/tmp/backtrace/* \
	      app/tmp/cache/* \
	$(RM) -r app/coverage

.PHONY: default run test test-js test-app deploy clean
