cptutils-online
---------------

Source for [cptutils-online][1], a small Sinatra app which
provides limited online access to the [cptutils][2] package.

[1]: http://seaviewsensing.com/pub/cptutils-online/
[2]: https://jjg.gitlab.io/en/code/cptutils/
